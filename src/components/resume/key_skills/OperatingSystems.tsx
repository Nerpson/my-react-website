import React from 'react';
import { FaDesktop } from 'react-icons/fa';

const OperatingSystems : React.FC = () => (
	<>
		<h5><FaDesktop /> Operating Systems</h5>
		<p>Windows, Linux, Mac OS</p>
	</>
);

export default OperatingSystems;