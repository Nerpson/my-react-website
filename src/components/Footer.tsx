import React, { Component } from 'react';

class Footer extends Component {
	render() { 
		return (
			<footer>
				<div className="gradient-line"></div>
				<p>Copyright, Nicolas Jullien, 2020</p>
			</footer>
		);
	}
}
 
export default Footer;