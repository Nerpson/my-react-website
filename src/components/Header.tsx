import React from 'react';
import { Link, withRouter } from 'react-router-dom'

const Header = () => (
	<header>
		<div className="cut-line auto-hide">
			<div id="header-box">
				<h1>Nicolas Jullien</h1>
				<nav>
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/creations">Creations</Link></li>
						<li><Link to="/resume">Resume</Link></li>
					</ul>
				</nav>
			</div>
		</div>
		<div className="gradient-line"></div>
	</header>
);

export default withRouter(Header);