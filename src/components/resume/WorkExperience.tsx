import React from 'react';
import { FaBriefcase } from 'react-icons/fa';
import WorkExperienceLine from './WorkExperienceLine';
import WorkExperienceProvider from '../../providers/WorkExperienceProvider';
import Window from '../Window';
import CollapseButton from '../CollapseButton';

class WorkExperience extends React.Component<{}, ResumePartState> {
	state = {
		collapsed: true
	};
	
	constructor(props: {}) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
		event.currentTarget.blur();
		this.setState({
			collapsed: !this.state.collapsed
		});
	}
	
	render() {
		const collapsedCount = 2;
		let experiences = WorkExperienceProvider.getAll();
		let displayCount = this.state.collapsed ? Math.min(collapsedCount, experiences.length) : experiences.length;
		
		return (
			<Window>
				<h2><FaBriefcase /> Work experience</h2>
				
				{ experiences.slice(0, displayCount).map(workExperienceItem => (
					<WorkExperienceLine key={workExperienceItem.description} workExperienceItem={workExperienceItem} />
				)) }
				<CollapseButton collapsed={this.state.collapsed} onClick={this.handleClick} count={experiences.length - collapsedCount} />
			</Window>
		);
	}
}

export default WorkExperience;