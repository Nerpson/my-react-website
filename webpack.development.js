const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	mode: 'development',
	watch: true,
	entry: './src/index.tsx',
	output: {
		filename: 'index_bundle.js',
		path: path.join(__dirname, '/dist')
	},
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
	},
	devtool: 'inline-source-map',
	module: {
		rules: [
      { test: /\.scss$/, use: [ "style-loader", "css-loader", "sass-loader" ] },
      { test: /\.jsx?$/, loader: "babel-loader" },
      { test: /\.tsx?$/, loader: "ts-loader" },
      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
			{
				test: /\.(png|jpe?g|svg)/,
				use: {
					loader: 'url-loader',
					options: {
						limit: 8000,
						name: 'images/[name].[hash].[ext]'
					}
				}
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html'
		}),
		new MiniCssExtractPlugin({
			filename: '[name].[hash].css',
			chunkFilename: '[id].[hash].css'
		})
	]
};