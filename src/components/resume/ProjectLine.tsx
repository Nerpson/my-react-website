import React from 'react';
import JobDetails from './JobDetails';
import JobDescription from './JobDescription';

interface ProjectLineProps {
	projectItem: ProjectItem;
}

const ProjectLine : React.FC<ProjectLineProps> = ({projectItem}) => (
	<div className="job clearfix">
		<JobDetails year={projectItem.year} location={projectItem.location} website={projectItem.website} />
		<JobDescription title={projectItem.title} description={projectItem.description} tags={projectItem.tags} />
	</div>
);

export default ProjectLine;