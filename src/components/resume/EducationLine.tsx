import React from 'react';
import JobDetails from './JobDetails';
import JobDescription from './JobDescription';

interface EducationLineProps {
	educationItem: EducationItem;
}

const EducationLine : React.FC<EducationLineProps> = ({educationItem}) => (
	<div className="job clearfix">
		<JobDetails year={educationItem.year} location={educationItem.location} website={educationItem.website} />
		<JobDescription title={educationItem.title} description={educationItem.description} />
	</div>
);

export default EducationLine;