import React from 'react';
import { FaPlus, FaMinus } from 'react-icons/fa';

type OnClickCallBack = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;

interface CollapseButtonProps {
	collapsed: boolean;
	onClick: OnClickCallBack;
	count: number;
}

const CollapseButton : React.FC<CollapseButtonProps> = ({collapsed, onClick, count}) => {
	let icon = collapsed ? <FaPlus /> : <FaMinus />;
	let text = collapsed ? "Show more (" + count + ")" : "Show less";
	let helper = collapsed ?
		"Click to show the " + (count > 1 ? count + " remaining elements" : "remaining element.") :
		"Click to hide the " + (count > 1 ? count + " last elements" : "last element.");
	
	return (
		<button className="collapser" onClick={onClick} title={helper}>
			{icon} {text}
		</button>
	);
};

export default CollapseButton;