import React from 'react';
import { FaClipboardList } from 'react-icons/fa';
import Window from '../Window';
import Development from './key_skills/Development';
import OperatingSystems from './key_skills/OperatingSystems';
import Softwares from './key_skills/Softwares';
import Languages from './key_skills/Languages';

const KeySkills = () => (
	<Window>
		<h2><FaClipboardList /> Key skills</h2>

		<Development />
		<OperatingSystems />
		<Softwares />
		<Languages />
		
	</Window>
);

export default KeySkills;