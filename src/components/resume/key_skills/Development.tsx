import React from 'react';
import { FaCode } from 'react-icons/fa';

const Development : React.FC = () => (
	<>
		<h5><FaCode /> Development</h5>
		<ul>
			<li>
				Software
				<p>C++, C, C#, Java, OpenGL</p>
			</li>
			<li>
				Web
				<p>HTML5, CSS3, PHP7, JavaScript, TypeScript</p>
				<p>Symfony, Yii, Node.js, React</p>
			</li>
			<li>
				Tools
				<p>Git, GitLab</p>
			</li>
		</ul>
	</>
);

export default Development;