type WorkExperienceItem = {
	company: string;
	description: string;
	year: string;
	location: string;
	website: string;
	tags: string[]
};