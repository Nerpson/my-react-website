import React from 'react';
import { Link } from 'react-router-dom';

interface JobDetailsProps {
	year: string;
	location: string;
	website: string;
}

const JobDetails : React.FC<JobDetailsProps> = (props: JobDetailsProps) => (
	<div className="job-details">
		<div className="date">{props.year}</div><div className="location">{props.location}</div>
		<div className="website"><a href={'https://' + props.website} title="Go to the website" target="blank">{props.website}</a></div>
	</div>
);

export default JobDetails;