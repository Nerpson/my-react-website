import React from 'react';
import { Helmet } from 'react-helmet';
import { FaRegFrown } from 'react-icons/fa';
import Window from './Window';

const Creations = () => (
	<>
		<Helmet>
			<title>Creations</title>
		</Helmet>

		<Window sm={true}>
			<h2><FaRegFrown /> Work in progress</h2>
			<p>This page will come very soon! Stay tuned.</p>
		</Window>
	</>
);

export default Creations;