# [nicolasjullien.fr](https://nicolasjullien.fr) website sources

You will find in this repository the source code that runs my own website, [nicolasjullien.fr](https://nicolasjullien.fr).

The technologies used are Node.js, React, Typescript, Webpack, Babel, SCSS, and many more.