import workExperiencesFileContent from './work_experiences.json';

export default class WorkExperienceProvider {
	static workExperiences : WorkExperienceItem[] = workExperiencesFileContent;

	static getAll() : WorkExperienceItem[] {
		return WorkExperienceProvider.workExperiences;
	}
}