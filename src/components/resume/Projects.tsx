import React from 'react';
import { FaBookmark } from 'react-icons/fa';
import ProjectProvider from '../../providers/ProjectProvider';
import ProjectLine from './ProjectLine';
import Window from '../Window';
import CollapseButton from '../CollapseButton';

class Projects extends React.Component<{}, ResumePartState> {
	state = {
		collapsed: true
	};

	constructor(props: {}) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
		event.currentTarget.blur();
		this.setState({
			collapsed: !this.state.collapsed
		});
	}

	render() {
		const collapsedCount = 2;
		let projects = ProjectProvider.getAll();
		let displayCount = this.state.collapsed ? Math.min(2, projects.length) : projects.length;
		
		return (
			<Window>
				<h2><FaBookmark /> Projects</h2>

				{ projects.slice(0, displayCount).map(projectItem => <ProjectLine key={projectItem.title} projectItem={projectItem} />) }
				<CollapseButton collapsed={this.state.collapsed} onClick={this.handleClick} count={projects.length - collapsedCount} />
			</Window>
		);
	}
}

export default Projects;