type ProjectItem = {
	title: string;
	description: string;
	year: string;
	location: string;
	website: string;
	tags: string[];
};