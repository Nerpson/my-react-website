import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'

import PersonalDetails from './resume/PersonalDetails';
import KeySkills from './resume/KeySkills';
import WorkExperience from './resume/WorkExperience';
import Education from './resume/Education';
import Projects from './resume/Projects';
import InterestsAndHobbies from './resume/InterestsAndHobbies';
import { Helmet } from 'react-helmet';

const Resume = () => (
	<>
		<Helmet>
			<title>Resume</title>
		</Helmet>
	
		<Row className="mx-3">
			<Col xl={3} lg={4}>
				<PersonalDetails />
				<KeySkills />
			</Col>
			<Col xl={9} lg={8}>
				<WorkExperience />
				<Education />
				<Projects />
				<InterestsAndHobbies />
			</Col>
		</Row>
	</>
);

export default Resume;