import React from 'react';
import { FaUser, FaPhone } from 'react-icons/fa';
import Window from '../Window';

const PersonalDetails = () => (
	<Window>
		<h2><FaUser /> Personal details</h2>

		<p>24 years old</p>
		<p>Driving licence</p>

		<h5><FaPhone /> Contact</h5>
		<p>me@nicolasjullien.fr</p>
	</Window>
);

export default PersonalDetails;