import React from 'react';
import { FaAngleRight } from 'react-icons/fa';
import ReactMarkdown from 'react-markdown';
import Tags from './Tags';

interface JobDescriptionProps {
	title: string;
	description: string;
	tags?: string[];
}

const JobDescription : React.FC<JobDescriptionProps> = ({title, description, tags}) => (
	<div className="job-description">
		<h5>
			<FaAngleRight /> <ReactMarkdown source={title} skipHtml={true} allowedTypes={['text', 'emphasis']} unwrapDisallowed={true} />
		</h5>
		<Tags tags={tags} />
		<ReactMarkdown source={description} escapeHtml={false} />
	</div>
);

export default JobDescription;