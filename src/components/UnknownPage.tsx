import React from 'react';
import { Helmet } from 'react-helmet';
import Window from './Window';

const UnknownPage = () => (
	<>
		<Helmet>
			<title>Unknown page</title>
		</Helmet>

		<Window><h2>Unknown page</h2></Window>
	</>
);

export default UnknownPage;