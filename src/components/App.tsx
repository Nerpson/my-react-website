import React from 'react';
import { Helmet } from 'react-helmet';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import '../styles/main.scss'

import Header from './Header';
import HomePage from './HomePage';
import Creations from './Creations';
import Resume from './Resume';
import UnknownPage from './UnknownPage';
import Footer from './Footer';

const App : React.FC = () => (
	<>
		<Helmet titleTemplate="%s - Nicolas Jullien" />

		<Router>
			<Header />
			<Switch>
				<Route exact path="/" component={HomePage} />
				<Route path="/creations" component={Creations} />
				<Route path="/resume" component={Resume} />
				<Route component={UnknownPage} />
			</Switch>
			<Footer />
		</Router>
	</>	
);

export default App;
