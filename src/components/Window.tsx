import React from 'react';

interface WindowProps {
	children: React.ReactNode;
	xl?: boolean;
	lg?: boolean;
	md?: boolean;
	sm?: boolean;
	xs?: boolean;
}

const Window : React.FC<WindowProps> = (props) => {
	let className = "wdw wdw-" + (
		props.xl ? "xl" :
		props.lg ? "lg" :
		props.md ? "md" :
		props.sm ? "sm" :
		props.xs
	);
	return (
		<div className={className}>
			<div className="cut-line">
				{props.children}
			</div>
		</div>
	);
};

export default Window;