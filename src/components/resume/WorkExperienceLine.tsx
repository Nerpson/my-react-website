import React from 'react';
import JobDetails from './JobDetails';
import JobDescription from './JobDescription';

interface WorkExperienceLineProps {
	workExperienceItem: WorkExperienceItem;
}

const WorkExperienceLine : React.FC<WorkExperienceLineProps> = ({workExperienceItem}) => {
	return (
		<div className="job clearfix">
			<JobDetails year={workExperienceItem.year} location={workExperienceItem.location} website={workExperienceItem.website} />
			<JobDescription title={workExperienceItem.company} description={workExperienceItem.description} tags={workExperienceItem.tags} />
		</div>
	);
};

export default WorkExperienceLine;