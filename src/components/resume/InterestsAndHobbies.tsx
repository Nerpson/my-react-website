import React from 'react';
import { FaHeart } from 'react-icons/fa';
import InterestProvider from '../../providers/InterestProvider';
import Window from '../Window';

const InterestsAndHobbies = () => (
	<Window>
		<h2><FaHeart /> Interests and hobbies</h2>
		
		{ InterestProvider.getAll().map(interest => <p key={interest}>{interest}</p>) }
	</Window>
);

export default InterestsAndHobbies;