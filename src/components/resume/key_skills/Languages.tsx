import React from 'react';
import { FaComment } from 'react-icons/fa';

const Languages : React.FC = () => (
	<>
		<h5><FaComment /> Languages</h5>
		<ul>
			<li>French (native speaker)</li>
			<li>English (advanced, TOEIC: 930)</li>
			<li>Spanish (basic knowledge)</li>
		</ul>
	</>
);

export default Languages;