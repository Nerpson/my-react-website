import React from 'react';
import { FaGripHorizontal } from 'react-icons/fa';

const Softwares : React.FC = () => (
	<>
		<h5><FaGripHorizontal /> Softwares</h5>
		<ul>
			<li>
				Programming
				<p>Visual Studio, IntelliJ, VS Code, ...</p>
			</li>
			<li>
				Audiovisual
				<p>Adobe Premiere</p>
			</li>
			<li>
				Graphism
				<p>Adobe Photoshop, InDesign</p>
			</li>
			<li>
				Microsoft Office
				<p>Word, Excel, Powerpoint</p>
			</li>
		</ul>
	</>
);

export default Softwares;