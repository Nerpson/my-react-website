import interestsFileContent from './interests.json';

export default class InterestProvider {
	static interests : string[] = interestsFileContent;

	static getAll() : string[] {
		return InterestProvider.interests;
	}
}