import React from 'react';
import { FaBook, FaAngleRight } from 'react-icons/fa';
import EducationLine from './EducationLine';
import EducationProvider from '../../providers/EducationProvider';
import Window from '../Window';
import CollapseButton from '../CollapseButton';

class Education extends React.Component<{}, ResumePartState> {
	state = {
		collapsed: true
	};
	
	constructor(props: {}) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
		event.currentTarget.blur();
		this.setState({
			collapsed: !this.state.collapsed
		});
	}

	render() {
		const collapsedCount = 2;
		let education = EducationProvider.getAll();
		let displayCount = this.state.collapsed ? Math.min(collapsedCount, education.length) : education.length;

		return (
			<Window>
				<h2><FaBook /> Education</h2>
				
				{ education.slice(0, displayCount).map(educationItem => (
					<EducationLine key={educationItem.title} educationItem={educationItem} />
				)) }
				<CollapseButton collapsed={this.state.collapsed} onClick={this.handleClick} count={education.length - collapsedCount} />
			</Window>
		);
	}
}

export default Education;