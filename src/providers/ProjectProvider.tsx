import projectsFileContent from './projects.json';

export default class ProjectProvider {
	static projects : ProjectItem[] = projectsFileContent;

	static getAll() : ProjectItem[] {
		return ProjectProvider.projects;
	}
}