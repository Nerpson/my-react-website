import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { FaEnvelope, FaPhone, FaCode, FaReact, FaGitlab, FaNodeJs } from 'react-icons/fa';
import { Helmet } from 'react-helmet';

class HomePage extends React.Component {
	static emojis = [
		"👀",
		"😏",
		"😳",
		"😯",
		"🤩",
		"😗",
		"😁",
		"😇"
	];

	constructor(props: {}) {
		super(props);
		this.update = this.update.bind(this);
	}

	update() {
		this.forceUpdate();
	}

	render() {
		const randomEmoji = HomePage.emojis[Math.floor(Math.random() * HomePage.emojis.length)];
		
		return (
			<>
				<Helmet>
					<title>Home</title>
				</Helmet>
			
				<Row className="mx-3">
					<Col md={8}>
						<div className="wdw">
							<div className="cut-line">
								<h1>Developer</h1>

								<div className="row mb-3">
									<div className="col-sm">
										<h4>Sofwares & video games</h4>
										<p>C++, Java, C#, OpenGL, ...</p>
									</div>
									<div className="col-sm">
										<h4>Web</h4>
										<p>PHP, Fullstack, Symfony, Yii2, JS, TS, React, ...</p>
									</div>
								</div>

								<h4>Some creations</h4>
								<p><strong>TMOne</strong> : Reboot of old Trackmania games</p>
								<p><strong>Planetless</strong> : Global Game Jam 2019 game made in 48 hours</p>
								<p><strong>WorldXGen</strong> : C++ simple mountain map generator</p>
							</div>
						</div>

						<div className="wdw">
							<div className="cut-line">
								<h1>Creator</h1>

								<div className="row">
									<div className="col-sm">
										<h4>Photography</h4>

										<p>Nature, landscapes, vegetation, ...</p>
										<a title="Instagram page" target="blank" href="https://instagram.com/nerpson">See on Instagram</a>
									</div>
									<div className="col-sm">
										<h4>Musics</h4>

										<p>EDM, Drum & Bass, Chill, ...</p>
										<a title="Soundcloud page" target="blank" href="https://soundcloud.com/nerpson">Listen on Soundcloud</a>
									</div>
									<div className="col-sm">
										<h4>Videos</h4>

										<p>Video games montages, contemplative videos, ...</p>
										<a title="Youtube page" target="blank" href="https://youtube.com/NerpsonGaming">Watch on Youtube</a>
									</div>
								</div>
							</div>
						</div>
					</Col>

					<Col md={4}>
						<div className="wdw profile-picture-bg"></div>

						<div className="wdw">
							<div className="cut-line">
								<h1>Contact me</h1>

								<p><FaEnvelope /> me@nicolasjullien.fr</p>
								<p><FaPhone /> Ask it by email ^^</p>
							</div>
						</div>

						<div className="wdw">
							<div className="cut-line">
								<h1>About this website</h1>

								<Row>
									<Col xl={8}>
										<p><FaNodeJs /> Project based on Node.js</p>
										<p><FaCode /> Coded in TypeScript</p>
										<p><FaReact /> Using the React framework</p>
										<p><FaGitlab /> Built &amp; deployed via GitLab CI/CD</p>
									</Col>
									<Col xl={4}>
										<p className="big-emoji d-none d-xl-block" title="Click me" onClick={this.update}>
											👈{randomEmoji}👍
										</p>
										<p className="big-emoji d-xl-none" title="Click me" onClick={this.update}>
											👆{randomEmoji}👍
										</p>
									</Col>
								</Row>
							</div>
						</div>
					</Col>
				</Row>
			</>
		);
	}
}

export default HomePage;