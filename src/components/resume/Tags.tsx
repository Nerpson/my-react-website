import React from 'react';

interface TagsProps {
	tags: string[];
}

const Tags : React.FC<TagsProps> = ({tags}) => {
	return (!tags || tags.length == 0)
		? null
		: (<p className="hashtags">{tags.map(tag => `#${tag} `)}</p>);
}

export default Tags;