type EducationItem = {
	title: string;
	description: string;
	year: string;
	location: string;
	website: string;
};

declare module "*.json"
{ const value: any;
  export default value;
}