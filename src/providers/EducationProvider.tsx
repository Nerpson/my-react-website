import educationFileContent from './education.json'

export default class EducationProvider {
	static educations: EducationItem[] = educationFileContent;
	
	static getAll() : EducationItem[] {
		return EducationProvider.educations;
	}
};
